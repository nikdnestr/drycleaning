import React, { FC, useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

export const Auth: FC = () => {
  const [isLogin, setIsLogin] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const history = useHistory();

  const handleSubmit = useCallback(() => {
    axios
      .post('http://localhost:4000/api/users/login', { email, password })
      .then((res) => {
        console.log(res.data.token);
        sessionStorage.setItem('token', res.data.token);
      });
  }, [email, password]);

  const handleOnChange = (e: any, setState: any) => {
    setState(e.target.value);
  };

  return (
    <div>
      <button
        onClick={() => {
          setIsLogin(!isLogin);
        }}
      >
        Switch
      </button>
      <div>{isLogin ? 'Login' : 'Signup'}</div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          handleSubmit();
          history.push('/services');
        }}
      >
        <label>
          Email:
          <input
            onChange={(e) => handleOnChange(e, setEmail)}
            type="text"
            name="name"
          />
        </label>
        <label>
          Password:
          <input
            onChange={(e) => handleOnChange(e, setPassword)}
            type="text"
            name="name"
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
      <button onClick={() => handleSubmit()}>test</button>
    </div>
  );
};
