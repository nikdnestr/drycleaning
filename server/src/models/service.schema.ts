import mongoose from 'mongoose';
import { IService } from './../interfaces/index';

const Schema = mongoose.Schema;

export const ServiceSchema = new Schema<IService>({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
});

export const Service = mongoose.model<IService>('Service', ServiceSchema);
