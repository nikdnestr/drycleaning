import mongoose from 'mongoose';
import { IServicePlace } from '../interfaces';
import { ServiceSchema } from './service.schema';

const Schema = mongoose.Schema;

const ServicePlaceSchema = new Schema<IServicePlace>({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  services: [
    {
      type: ServiceSchema,
      ref: 'Service',
    },
  ],
});

export const ServicePlace = mongoose.model<IServicePlace>(
  'ServicePlace',
  ServicePlaceSchema
);
