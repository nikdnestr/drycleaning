import { NextFunction, Request, Response } from 'express';
import bcrypt from 'bcrypt';
import { User } from '../models/user.schema';
import signJWT from '../helper/signJWT';

// const validateToken = (req: Request, res: Response, next: NextFunction) => {
//   if (!res.locals.user.admin) {
//     return res.status(400).json({ message: 'Restricted route' });
//   }
//   return res.status(200).json({ message: 'Welcome' });
// };

const signup = (req: Request, res: Response, next: NextFunction) => {
  let { email, password, admin } = req.body;

  bcrypt.hash(password, 10, (hashError, hash) => {
    if (hashError) {
      return res.status(401).json({
        message: hashError.message,
        error: hashError,
      });
    }

    const newUser = new User({
      email,
      password: hash,
      admin,
    });

    return newUser
      .save()
      .then((user) => {
        return res.status(201).json({
          user,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: error.message,
          error,
        });
      });
  });
};

const login = (req: Request, res: Response, next: NextFunction) => {
  let { email, password } = req.body;

  User.findOne({ email })
    .then((user) => {
      if (!user) {
        res.status(404).json({ error: 'Email not found' });
      } else {
        bcrypt.compare(password, user.password, (e, match) => {
          if (e) {
            res.status(500).json(e);
          } else if (match) {
            signJWT(user, (error, token) => {
              if (error) {
                return res.status(500).json({
                  message: error.message,
                  error,
                });
              } else if (token) {
                return res.status(200).json({
                  message: 'Auth successful',
                  token: token,
                  user,
                });
              }
            });
          } else {
            res.status(403).json({ message: 'Wrong password' });
          }
        });
      }
    })
    .catch((e) => res.status(500).json(e));
};

const changePassword = (req: Request, res: Response, next: NextFunction) => {
  let { email, password, newPassword } = req.body;

  User.findOne({ email })
    .then((user) => {
      if (!user) {
        res.status(404).json({ error: 'Email not found' });
      } else {
        bcrypt.compare(password, user.password, (e, match) => {
          if (e) {
            res.status(500).json(e);
          } else if (match) {
            bcrypt.hash(newPassword, 10, (hashError, hash) => {
              if (hashError) {
                return res.status(401).json({
                  message: hashError.message,
                  error: hashError,
                });
              }
              User.findOneAndUpdate({ email }, { password: hash }).then(
                (user) => res.status(200).json(user)
              );
            });
          } else {
            res.status(403).json({ message: 'Wrong password' });
          }
        });
      }
    })
    .catch((e) => res.status(500).json(e));
};

const changeBalance = (req: Request, res: Response, next: NextFunction) => {
  let { email, newValue } = req.body;
  User.findOneAndUpdate({ email }, { balance: newValue })
    .then(() =>
      User.findOne({ email }).then((user) => res.status(200).json(user))
    )
    .catch((e) => res.status(500).json(e));
};

const getAllUsers = (req: Request, res: Response, next: NextFunction) => {
  if (!res.locals.user.admin) {
    return res.status(401).json({ message: 'Restricted route' });
  }
  User.find({})
    .then((users) => {
      return res.status(200).json({
        users,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: error.message,
        error,
      });
    });
};

const removeUser = (req: Request, res: Response, next: NextFunction) => {
  let { email } = req.body;
  if (!res.locals.user.admin) {
    if (res.locals.user.email !== req.body.email) {
      return res.status(401).json({ message: 'Restricted route' });
    }
    User.findOneAndDelete({ email }).then((user) => res.status(200).json(user));
    return;
  }
  User.findOneAndDelete({ email }).then((user) => res.status(200).json(user));
};

export {
  signup,
  login,
  changePassword,
  changeBalance,
  getAllUsers,
  removeUser,
};
