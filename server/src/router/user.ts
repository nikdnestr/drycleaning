import express from 'express';
import {
  signup,
  login,
  changePassword,
  changeBalance,
  getAllUsers,
  removeUser,
} from '../controllers/user.controller';
import { extractJWT } from '../middleware/extractJWT';

export const router = express.Router();

router.get('/', extractJWT, getAllUsers);
router.post('/signup', signup);
router.post('/login', login);
router.put('/changePassword', changePassword);
router.patch('/changeBalance', extractJWT, changeBalance);
router.delete('/', extractJWT, removeUser);
