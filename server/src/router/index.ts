import { router as servicePlace } from './servicePlace';
import { router as user } from './user';
import { router as order } from './order';

export { servicePlace, user, order };
