import express from 'express';
import {
  getOrders,
  getOrder,
  makeOrder,
  editOrder,
  removeOrder,
} from '../controllers/order.controller';
import { extractJWT } from '../middleware/extractJWT';

export const router = express.Router();

router.get('/', extractJWT, getOrders);
router.get('/:id', extractJWT, getOrder);
router.post('/', extractJWT, makeOrder);
router.put('/:id', extractJWT, editOrder);
router.delete('/:id', extractJWT, removeOrder);
