import jwt from 'jsonwebtoken';
import config from '../config';
import { IUser } from '../interfaces';

const signJWT = (
  user: IUser,
  callback: (error: Error | null, token: string | null) => void
): void => {
  // var timeSinceEpoch = new Date().getTime();
  // var expirationTime = timeSinceEpoch + Number(config.token.time) * 100000;
  // var expirationTimeInSeconds = Math.floor(expirationTime / 1000);

  try {
    jwt.sign(
      {
        email: user.email,
        admin: user.admin,
      },
      config.token.secret,
      {
        issuer: config.token.issuer,
        algorithm: 'HS256',
        expiresIn: 21600,
      },
      (error, token) => {
        if (error) {
          callback(error, null);
        } else if (token) {
          callback(null, token);
        }
      }
    );
  } catch (error: any) {
    callback(error, null);
  }
};

export default signJWT;
