import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import config from './config';
import { user, servicePlace, order } from './router';

const app = express();

mongoose.connect('mongodb://localhost/dry-cleaning');
mongoose.Promise = global.Promise;

app.use(express.static('public'));
app.use(cors({ origin: 'http://localhost:3000' }));
app.use(express.json());
app.use('/api/users', user);
app.use('/api/servicePlaces', servicePlace);
app.use('/api/orders', order);

app.listen(config.port, () => {
  console.log(`Up on localhost:${config.port}`);
});
