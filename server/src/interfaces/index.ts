import { Document } from 'mongoose';

export interface IUser extends Document {
  email: string;
  password: string;
  balance: number;
  admin: boolean;
  createdAt: Date;
  updatedAt: Date;
}

export interface IOrder extends Document {
  name: string;
  service: string;
  price: number;
}

export interface IService extends Document {
  name: string;
  description: string;
  price: number;
  status: string;
}

export interface IServicePlace extends Document {
  name: string;
  description: string;
  services: [IService];
}
